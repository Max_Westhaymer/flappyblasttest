﻿using UnityEngine;
using System.Collections;

public class TransformMove : MonoBehaviour {
	Vector3 velocity = Vector3.zero;
	public Vector3 gravity;
	public Vector3 flapVelocity ;
	public float maxSpeed = 5f;

	bool didFlap = false;

	Animator animator;

	public bool dead = false;
	float deathCooldown;

	public bool godMode = false;

	// Use this for initialization
	// Use this for initialization
	void Start () {
	
	}
	void Update() {
		
			if(Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0) ) {
			
				didFlap = true;
			}

	}
	void FixedUpdate () {

		velocity.x = maxSpeed;
		velocity += gravity * Time.deltaTime;

		if(didFlap == true) {
			velocity += flapVelocity;
			didFlap = false;
		}
		velocity = Vector3.ClampMagnitude (velocity, maxSpeed);
		transform.position += velocity * Time.deltaTime;

		//		if(GetComponent<Rigidbody2D>().velocity.y > -0.25f) {
		//			transform.rotation = Quaternion.Euler(0, 0, 0);
		//		}
		//		else {
		//			float angle = Mathf.Lerp (0, -90, (-GetComponent<Rigidbody2D>().velocity.y / 3f) );
		//			transform.rotation = Quaternion.Euler(0, 0, angle);
		//		}
	}
}
