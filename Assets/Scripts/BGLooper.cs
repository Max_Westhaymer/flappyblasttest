﻿using UnityEngine;
using System.Collections;

public class BGLooper : MonoBehaviour {

	int numBGPanels = 6;

	 public float[] _Yvalues;
	void Start() {
		GameObject[] pipes = GameObject.FindGameObjectsWithTag("Pipe");

		foreach(GameObject pipe in pipes) {
			Vector3 pos = pipe.transform.position;
			pos.y = _Yvalues [Random.Range(0, _Yvalues.Length)];
			pipe.transform.position = pos;
		}
		Debug.Log (_Yvalues[0]);
	}

	void OnTriggerEnter2D(Collider2D collider) {
		Debug.Log ("Triggered: " + collider.name);

		float widthOfBGObject = ((BoxCollider2D)collider).size.x;

		Vector3 pos = collider.transform.position;

		pos.x += widthOfBGObject * numBGPanels;

		if(collider.tag == "Pipe") {
			pos.y = _Yvalues [Random.Range(0, _Yvalues.Length)];
		}

		collider.transform.position = pos;

	}
}
