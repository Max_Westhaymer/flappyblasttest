﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
public class BlastMove : MonoBehaviour {

		Vector3 velocity = Vector3.zero;
		public Vector3 gravity;
		public Vector3 flapVelocity;
		public bool didFlap = false;
		public float _maxSpeed;
		public float _forwardSpeed = 1f;
	public Text _Yvelocity;
	//Animator animator;

	public bool dead = false;
	float deathCooldown;
     bool _collision = false;
	GameObject _music;
	public bool godMode = false;
	public HugeDoTweener _tweener;
	// Use this for initialization
	void Start () {
		_music = GameObject.FindGameObjectWithTag("Music");
		_tweener.ShakeCam (1f, 0.1f, 10);
		
		//animator = transform.GetComponentInChildren<Animator>();

		//if(animator == null) {
		//	Debug.LogError("Didn't find animator!");
		//}
		if (_music != null) {
			_music.GetComponent<AudioSource> ().DOPitch (1f, 1f);
		}
	}
		void Update()
	{
		_Yvelocity.text = "Y vilocity = " + velocity.y;
		if(dead) {
			deathCooldown -= Time.deltaTime;

			if(deathCooldown <= 0) {
				if(Input.GetMouseButtonDown(0)) {
					Application.LoadLevel( Application.loadedLevel );
				}
			}
		}
		else {
			if(Input.GetMouseButton(0)) {
				didFlap = true;
			}
			else didFlap = false;
		}
		}
	// Update is called once per frame
	void FixedUpdate () {
		if (_collision)
			return;
				velocity.x = _forwardSpeed;
				velocity += gravity * Time.deltaTime;
			if (didFlap == true) 
		{
						//didFlap = false;
						if (velocity.y < 0)
								velocity.y = 0;
						velocity += flapVelocity;								
		}
				velocity = Vector3.ClampMagnitude (velocity, _maxSpeed);
				transform.position += velocity * Time.deltaTime;
		//float angle = Mathf.Lerp (0, 0, velocity.y / _maxSpeed);
		//if (velocity.y < 0) {
		//	angle = Mathf.Lerp (0, -90, -velocity.y / _maxSpeed);
		//} 
				//transform.rotation = Quaternion.Euler (0, 0, angle);
	}
	void OnTriggerEnter2D(Collider2D collision) {
		if (collision.tag == "Death") {
			Debug.Log ("dsfdfsdf");
			if (godMode)
				return;
			_collision = true;
			//animator.SetTrigger ("Death");
			dead = true;
			if (_music != null) {
				_music.GetComponent<AudioSource> ().DOPitch (0f, 1f);
			}
			deathCooldown = 0.5f;
			_tweener.ShakeCam (1f,0.1f,10);
		}
	}
	public void OnDown()
	{
		didFlap = true;
	}
}
