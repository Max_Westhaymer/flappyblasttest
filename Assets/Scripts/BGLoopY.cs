﻿using UnityEngine;
using System.Collections;

public class BGLoopY : MonoBehaviour {
	public float SpeedOffset;
	public GameObject Player;

	public bool PlayerOrSpeedOffset = true;
	void FixedUpdate () {
		MeshRenderer mr = GetComponent<MeshRenderer> ();
		Material mt = mr.material;
		Vector2 offset = mt.mainTextureOffset;
		if(PlayerOrSpeedOffset == true)
			offset.y = Player.transform.position.y / SpeedOffset;
		else if(PlayerOrSpeedOffset == false)
			offset.y += Time.deltaTime / SpeedOffset;	
		mt.mainTextureOffset = offset;
	}
}
