﻿using UnityEngine;
using System.Collections;

public class OffsetBGLoop : MonoBehaviour {
	public float SpeedOffset;
	public GameObject Player;

	public bool PlayerOrSpeedOffset = true;
	void FixedUpdate () {
		MeshRenderer mr = GetComponent<MeshRenderer> ();
		Material mt = mr.material;
		Vector2 offset = mt.mainTextureOffset;
		if(PlayerOrSpeedOffset == true)
		offset.x = Player.transform.position.x / SpeedOffset;
		else if(PlayerOrSpeedOffset == false)
		offset.x += Time.deltaTime / SpeedOffset;	
		mt.mainTextureOffset = offset;
	}
}
