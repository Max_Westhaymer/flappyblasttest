﻿using UnityEngine;
using System.Collections;

public class Parrallaxed  : MonoBehaviour
{
    public float Ratio = 1f;
    private Vector3 _startPosition;
    public Transform CameraObject;
    public bool GarbageCollector = true;

    private void OnEnable() 
	{
		_startPosition = transform.position;
	}

    void LateUpdate()
    {
        Doit();
        if (Time.frameCount % 30 == 0 && GarbageCollector) System.GC.Collect();
    }

    private void Doit()
    {
        if (CameraObject == null) return;
        var position = transform.position;
        position.x = CameraObject.position.x * Ratio + _startPosition.x;

        transform.position = position;
    }
}
