﻿using UnityEngine;
using System.Collections;

public class DestroyerSpawnPrefabs : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.tag == "Death") {
			Debug.Log ("Префаб удален");
			Destroy (coll.gameObject);
		}
	}
}
