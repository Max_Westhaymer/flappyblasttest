﻿using UnityEngine;
using System.Collections;

public class SpawnObjOneNew : MonoBehaviour {

	public GameObject[] spawnObjectPrefabs;
	[Space(7)]
	[Header("Параметры спавна")]
	public Vector2 spawnMinMaxSpawn = new Vector2(0,5);
	[Space(5)]
	public bool RandomScale = true;
	public Vector2 spawnMinMaxScale = new Vector2 (1,5);
	[Space(5)]
	public bool RandomPosition = true;
	public Vector2 spawnUpDownRand = new Vector2 (-4,4);
	[Space(7)]
	public bool RandomRotation = true;
	[Range(0, 360)]
	public float spawnAngleRandMax = 360;


	void Start () {
		StartCoroutine (SpawnIe());
	}
	public IEnumerator SpawnIe() {
		yield return new WaitForSeconds(Random.Range (spawnMinMaxSpawn.x, spawnMinMaxSpawn.y));

		Debug.Log ("Заспавнил");

		float randomCoordinate = 0;
		if (RandomPosition) {
			randomCoordinate = Random.Range (spawnUpDownRand.x, spawnUpDownRand.y);
		}

		GameObject spawnObj = Instantiate (spawnObjectPrefabs[Random.Range(0,spawnObjectPrefabs.Length)] , new Vector2 (transform.position.x, transform.position.y + randomCoordinate), Quaternion.identity) as GameObject; 

		if (RandomScale) {
			float scaleSize = Random.Range (spawnMinMaxScale.x, spawnMinMaxScale.y);
			spawnObj.transform.localScale = new Vector3 (scaleSize, scaleSize, scaleSize);
		}
		if (RandomRotation) {
			float AngleRand = Random.Range (0, spawnAngleRandMax);
			spawnObj.transform.rotation = Quaternion.Euler (0, 0, AngleRand);
		}

		StartCoroutine (SpawnIe());
	}

	public void StartCoroutineFun()
	{
		StartCoroutine (SpawnIe());
	}

}