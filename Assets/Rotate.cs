﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Rotate : MonoBehaviour {

	// Use this for initialization
	void Start () {
		transform.DOLocalRotate (new Vector3 (0, 0, -1), 2, RotateMode.FastBeyond360).SetLoops (-1, LoopType.Incremental);
	}
	

}
