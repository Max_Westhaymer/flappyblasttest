﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;
public class HugeDoTweener : MonoBehaviour {
	public Camera _camera;
	public Camera _camera2;
	public Canvas _canva;
	public void ShakeCam (float _dur, float _str, int _vibr)
	{
		_camera.DOShakePosition(_dur,_str,_vibr,90f).SetUpdate(UpdateType.Fixed);
		_camera2.DOShakePosition(_dur,_str,_vibr,90f).SetUpdate(UpdateType.Fixed);
	}
}
