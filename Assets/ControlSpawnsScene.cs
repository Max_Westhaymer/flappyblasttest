﻿using UnityEngine;
using System.Collections;

public class ControlSpawnsScene : MonoBehaviour {

	public GameObject[] Spawners;
	public float[] timersActivate;
	public float currentSpawnerTimer;
	public int numSpawnIndex = 0;
	public bool LoopSwichSpawners = false;

	void Start () {
		
		currentSpawnerTimer = timersActivate [numSpawnIndex];
		//Выключаем все спаунеры, кроме первого
		if(Spawners.Length > 1)
		{
			for (int j = 1; j < Spawners.Length; j++) {
				Spawners [j].SetActive (false);
			}
		}
	}
	void FixedUpdate () {
		
		currentSpawnerTimer -= Time.deltaTime;
		if (currentSpawnerTimer <= 0) { //Меняем спаунер, проверяем не кончился ли массив, если кончился - не выполняем смену
			if (numSpawnIndex + 1 == Spawners.Length && LoopSwichSpawners == true) { //Если включено зацикливание - то сброс на нулевой спавнер
				Spawners [numSpawnIndex].SetActive (false);
				numSpawnIndex = 0;
				Spawners [numSpawnIndex].SetActive (true);
				Spawners [numSpawnIndex].GetComponent<SpawnObjectCoroutine> ().StartCoroutineFun ();

				currentSpawnerTimer = timersActivate [numSpawnIndex];
				return;
			} else if(numSpawnIndex + 1 == Spawners.Length && LoopSwichSpawners == false){
				Debug.Log ("Спавнеры кончились, зацикливание не активно");
				return;
			}
			Spawners [numSpawnIndex].SetActive (false);
			numSpawnIndex++;
			Spawners [numSpawnIndex].SetActive (true);
			Spawners [numSpawnIndex].GetComponent<SpawnObjectCoroutine> ().StartCoroutineFun ();
			currentSpawnerTimer = timersActivate [numSpawnIndex];

		}
	}
}
